#pragma once

#include <iostream>

int square_sum(int a, int b) {
    return (a + b) * (a + b);
}